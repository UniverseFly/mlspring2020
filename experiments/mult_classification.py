import numpy as np
import matplotlib.pyplot as plt
from linear_models.multiple_classification_model import MultipleClassificationModel


def test():
    # 在一个长方形方形区域内随机生成 count 个点
    def gen_random_square(start_x, start_y, end_x, end_y, count):
        return np.append(np.random.uniform(start_x, end_x, count).reshape(-1, 1),
                         np.random.uniform(start_y, end_y, count).reshape(-1, 1),
                         axis=1)

    class Data:
        class_a = gen_random_square(0, 0, 5, 5, 10)
        class_b = gen_random_square(3, 3, 8, 8, 10)
        class_c = gen_random_square(7, 0, 12, 5, 10)

        X_a = np.concatenate((np.ones(class_a.shape[0]).reshape(-1, 1), class_a), axis=1)
        X_b = np.concatenate((np.ones(class_b.shape[0]).reshape(-1, 1), class_b), axis=1)
        X_c = np.concatenate((np.ones(class_c.shape[0]).reshape(-1, 1), class_c), axis=1)

        y_a = np.repeat(0, class_a.shape[0])
        y_b = np.repeat(1, class_b.shape[0])
        y_c = np.repeat(2, class_c.shape[0])

        X = np.concatenate((X_a, X_b, X_c))
        y = np.concatenate((y_a, y_b, y_c))

    model = MultipleClassificationModel(
        Data.X, Data.y,
        reg_lambda=0.02
    )

    def plot_truth():
        plt.scatter(Data.class_a[:, 0], Data.class_a[:, 1], color="orange", label="category 0")
        plt.scatter(Data.class_b[:, 0], Data.class_b[:, 1], color="blue", label="category 1")
        plt.scatter(Data.class_c[:, 0], Data.class_c[:, 1], color="green", label="category 2")

    print("图中显示了训练数据")
    plot_truth()
    plt.legend()
    plt.title("Training data")
    plt.show()

    model.basis_func = lambda x: x
    model.learn(learning_rate=0.3, epochs=500)

    def plot_decision_boundaries():
        def plot_single_decision_boundary(model, color):
            u = np.linspace(0, 12, 80)
            v = np.linspace(0, 8, 80)

            z = np.zeros((u.size, v.size))
            # Evaluate z = theta*x over the grid
            for i, ui in enumerate(u):
                for j, vj in enumerate(v):
                    mapped_value = model.basis_func(np.array([1, ui, vj]))
                    std_right_side = (mapped_value[1:] - model.mean) / model.std
                    first_column = mapped_value[0]
                    mapped_value = np.append(first_column, std_right_side)
                    z[i, j] = np.dot(mapped_value, model.params)

            z = z.T
            plt.contour(u, v, z, levels=[0], linewidths=2, colors=[color])

        plot_truth()
        plot_single_decision_boundary(model.learned_models[0][0], "red")
        plot_single_decision_boundary(model.learned_models[1][0], "purple")
        plot_single_decision_boundary(model.learned_models[2][0], "orange")

        plt.legend()
        plt.title("Decision boundaries")
        plt.show()

    print("对模型进行训练，并绘出决策边界（这里基本函数是恒等映射，所以决策边界线性）")
    plot_decision_boundaries()

    model.basis_func = lambda x: np.array([1,
                                           x[1],
                                           x[2],
                                           x[1] ** 2,
                                           x[2] ** 2,
                                           x[1] * x[2],
                                           x[1] ** 3,
                                           x[2] ** 3
                                           ])
    model.learn(learning_rate=0.3, epochs=500)

    print("对模型进行训练，并绘出决策边界（这里基本函数是多维映射，所以决策边界非线性）")
    plot_decision_boundaries()
