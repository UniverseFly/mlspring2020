import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from linear_models.binary_classification_model import RegularizedBinaryLogisticRegression


class Data:
    _X = pd.read_csv("data/ex2data2.txt").values

    # 在第一列补充 1
    X = np.concatenate((np.ones(_X.shape[0]).reshape(-1, 1), _X[:, :-1]), axis=1)
    y = _X[:, -1]

    positive = np.array(list(filter(lambda x: x[2] == 1, _X)))
    negative = np.array(list(filter(lambda x: x[2] == 0, _X)))

    @staticmethod
    def basis_func(x):
        out = [1.0]
        # 多项式的阶数，可以自由修改进行实验
        degree = 10
        for i in range(1, degree + 1):
            for j in range(i + 1):
                out.append((x[1] ** (i - j)) * (x[2] ** j))
        return np.array(out)


def test():
    model = RegularizedBinaryLogisticRegression(
        Data.X, Data.y,
        basis_func=Data.basis_func,
        reg_lambda=0
    )

    print("模型的初始梯度和代价如下：")
    print("梯度:", model.gradient())
    print("代价:", model.cost())

    def plot_truth():
        plt.scatter(Data.positive[:, 0], Data.positive[:, 1], color="blue", label="Positive")
        plt.scatter(Data.negative[:, 0], Data.negative[:, 1], color="orange", label="Negative")

    print("图中显示了训练集")
    plot_truth()
    plt.legend()
    plt.title("Training data")
    plt.show()

    def plot_decision_boundary():
        u = np.linspace(-1, 1.5, 50)
        v = np.linspace(-1, 1.5, 50)

        z = np.zeros((u.size, v.size))
        # Evaluate z = theta*x over the grid
        for i, ui in enumerate(u):
            for j, vj in enumerate(v):
                mapped_value = model.basis_func(np.array([1, ui, vj]))
                std_right_side = (mapped_value[1:] - model.mean) / model.std
                first_column = mapped_value[0]
                mapped_value = np.append(first_column, std_right_side)
                z[i, j] = np.dot(mapped_value, model.params)

        z = z.T

        plt.contour(u, v, z, levels=[0], linewidths=2, colors='r')

    model.params = np.zeros(model.params.size)
    model.learn_by_gradient_decent(epochs=50000, learning_rate=3)

    print("图中显示了一个过拟合模型的决策边界")
    plot_truth()
    plot_decision_boundary()
    plt.legend()
    plt.title("Overfitted model")
    plt.show()

    model.params = np.zeros(model.params.size)
    model.reg_lambda = 2
    model.learn_by_gradient_decent(epochs=50000, learning_rate=3)

    print("图中显示了一个正则化后模型的决策边界（lambda = 2）")
    plot_truth()
    plot_decision_boundary()
    plt.legend()
    plt.title("Model with lambda = {}".format(model.reg_lambda))
    plt.show()

    model.params = np.zeros(model.params.size)
    model.reg_lambda = 25
    model.learn_by_gradient_decent(epochs=50000, learning_rate=3)

    print("图中显示了一个欠拟合模型的决策边界（lambda = 25）")
    plot_truth()
    plot_decision_boundary()
    plt.legend()
    plt.title("Underfitted model with lambda = {}".format(model.reg_lambda))
    plt.show()
