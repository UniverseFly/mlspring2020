import numpy as np
import matplotlib.pyplot as plt
from linear_models.regression_model import RegularizedLinearRegression


class Data:
    _range = np.arange(0, 1, 0.1)

    # 在第一列补充 1
    X = np.concatenate((np.ones(_range.shape[0]).reshape(-1, 1), _range.reshape(-1, 1)), axis=1)
    # sin(x) 以及随机噪声
    y = np.sin(2 * np.pi * _range) + np.random.normal(0, 0.3, _range.size)

    @staticmethod
    # 多项式回归，一个十次多项式
    def basis_func(x):
        return np.array([
            1,
            x[1],
            x[1] ** 2,
            x[1] ** 3,
            x[1] ** 4,
            x[1] ** 5,
            x[1] ** 7,
            x[1] ** 8,
            x[1] ** 9,
            x[1] ** 10,
        ])


def test():
    def plot_truth():
        plt.scatter(Data.X[:, 1], Data.y, label="Ground Truth")

    print("图中显示了训练集数据")
    plot_truth()
    plt.title("y = sin(x) + N(x | 0, 0.3)")
    plt.show()

    model = RegularizedLinearRegression(
        training_set=Data.X,
        target_set=Data.y,
        basis_func=Data.basis_func,
        reg_lambda=0
    )

    print("模型开始的梯度和损失如下：")
    print("梯度:", model.gradient())
    print("损失:", model.cost())

    # %%

    # 一个工具函数，可以画出预测值
    def plot_prediction(input_data, prediction, title):
        plt.plot(input_data, prediction, "r")
        plt.title(title)

    # 测试结果的范围，由于原始数据最后是0.9这个点，所以这里的范围这样子写
    input_range = np.arange(0, 0.91, 0.01)

    # 正则化参数 lambda
    model.reg_lambda = 0.0
    model.learn_by_normal_equation()
    overfit_prediction = np.array([model.prediction(np.append(1, x)) for x in input_range])

    print("图中显示了真实结果与过拟合的预测结果")
    # 画出真实结果以及预测结果
    plot_truth()
    plot_prediction(input_range, overfit_prediction, "Overfitted model without regularization")
    plt.legend(["Ground Truth", "Prediction"])
    plt.show()

    model.reg_lambda = 0.01
    model.learn_by_normal_equation()
    good_prediction = np.array([model.prediction(np.append(1, x)) for x in input_range])

    print("图中显示了真实结果与经过正则化的预测结果（lambda = 0.01）")
    plot_truth()
    plot_prediction(input_range, good_prediction, "Model with lambda = {}".format(model.reg_lambda))
    plt.legend(["Ground Truth", "Prediction"])
    plt.show()

    model.reg_lambda = 10
    model.learn_by_normal_equation()
    underfit_prediction = np.array([model.prediction(np.append(1, x)) for x in input_range])

    print("图中显示了欠拟合的结果（lambda = 10）")
    plot_truth()
    plot_prediction(input_range, underfit_prediction, "Underfitted model with lambda = {}".format(model.reg_lambda))
    plt.legend(["Ground Truth", "Prediction"])
    plt.show()

    # 需要重置一下参数，否则在实验过程中如果learning_rate太大会让参数爆炸
    model.params = np.zeros(model.params.size)
    # 设置正则化参数
    model.reg_lambda = 0.001
    # 传入梯度下降的学习率以及迭代次数，学习率过大会导致代价函数增加
    model.learn_by_gradient_decent(learning_rate=0.1, epochs=300)
    gradient_learn_prediction = np.array([model.prediction(np.append(1, x)) for x in input_range])

    print("图中显示了梯度下降的训练结果，学习率为 0.1，迭代次数为 300")
    plot_truth()
    plot_prediction(input_range, gradient_learn_prediction,
                    "Gradient decent with lambda = {}".format(model.reg_lambda))
    plt.show()

    print("图中显示了代价函数在训练过程中的变化情况")
    # 展示代价函数在训练过程中的变化情况
    plt.plot(model.cost_history)
    plt.xlabel("epoch")
    plt.ylabel("cost")
    plt.title("Cost change through learning epoch")
    plt.show()
