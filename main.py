from experiments.regression import test as test1
from experiments.bin_classification import test as test2
from experiments.mult_classification import test as test3

print("正在进行的是线性回归问题的实验")
test1()

print("\n正在进行的是二分类逻辑回归问题的实验")
test2()

print("\n正在进行的是多分类逻辑回归问题的实验")
test3()
