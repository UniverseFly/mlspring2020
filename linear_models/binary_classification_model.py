import numpy as np


class RegularizedBinaryLogisticRegression:
    def __init__(self,
                 training_set: np.array,
                 target_set: np.array,
                 reg_lambda: float = 0.0,
                 basis_func=lambda x: x):
        self.basis_func = basis_func
        self.training_set = training_set

        self.mapped_training_set = np.array([self.basis_func(x) for x in training_set])
        self.mean = np.mean(self.mapped_training_set[:, 1:], axis=0)
        self.std = np.std(self.mapped_training_set[:, 1:], axis=0)

        # 进行标准化
        standardized = (self.mapped_training_set[:, 1:] - self.mean) / self.std
        first_column = self.mapped_training_set[:, 0].reshape(-1, 1)
        self.mapped_training_set = np.concatenate((first_column, standardized), axis=1)

        self.target_set = target_set
        # shape[1] 代表了矩阵的列数
        self.params = np.zeros(self.mapped_training_set.shape[1])
        self.reg_lambda = reg_lambda

    @staticmethod
    def sigmoid(z):
        return 1 / (1 + np.exp(-z))

    @staticmethod
    def pair_cost(prediction, reality):
        return -reality * np.log(prediction) - (1 - reality) * np.log(1 - prediction)

    def prediction(self, input_data):
        return 1 if self.probability(input_data) >= 0.5 else 0

    def probability(self, input_data):
        non_std_input = self.basis_func(input_data)

        # 标准化
        first_column = non_std_input[0]
        standardized = (non_std_input[1:] - self.mean) / self.std
        std_input = np.append(first_column, standardized)

        return self.sigmoid(np.dot(self.params, std_input))

    def cost(self) -> float:
        X = self.mapped_training_set
        m = X.shape[0]
        y = self.target_set
        w = self.params

        data_pairs = zip(X, y)
        prediction_cost = sum(self.pair_cost(self.sigmoid(np.dot(w, x)), y) for x, y in data_pairs) / m

        # prediction_cost = sum(prediction_diff) / m
        reg_cost = self.reg_lambda * np.dot(w, w) / (2 * m)

        return prediction_cost + reg_cost

    def gradient(self):
        X = self.mapped_training_set
        m = X.shape[0]
        y = self.target_set
        w = self.params

        # print(X)

        primary_grad = np.dot(X.transpose(), self.sigmoid(np.dot(X, w)) - y) / m
        reg_grad = (self.reg_lambda / m) * w

        # print(primary_grad, reg_grad)

        return primary_grad + reg_grad

    def learn_by_gradient_decent(self, learning_rate=0.01, reg_lambda=0, epochs=100):
        for epoch in range(epochs):
            self.params -= learning_rate * self.gradient()
