import numpy as np


class RegularizedLinearRegression:
    def __init__(self,
                 training_set: np.array,
                 target_set: np.array,
                 reg_lambda: float = 0.0,
                 basis_func=lambda x: x):
        self.basis_func = basis_func
        self.training_set = training_set

        self.mapped_training_set = np.array([np.array(self.basis_func(x)) for x in training_set])
        self.mean = np.mean(self.mapped_training_set[:, 1:], axis=0)
        self.std = np.std(self.mapped_training_set[:, 1:], axis=0)

        # 进行标准化
        standardized = (self.mapped_training_set[:, 1:] - self.mean) / self.std
        first_column = self.mapped_training_set[:, 0].reshape(-1, 1)
        self.mapped_training_set = np.concatenate((first_column, standardized), axis=1)

        self.target_set = target_set
        # shape[1] 代表了矩阵的列数
        self.params = np.zeros(self.mapped_training_set.shape[1])
        self.reg_lambda = reg_lambda

        # 梯度下降的历史记录
        self.cost_history = []

    def prediction(self, input_data):
        non_std_input = self.basis_func(input_data)

        # 标准化
        first_column = non_std_input[0]
        standardized = (non_std_input[1:] - self.mean) / self.std
        std_input = np.append(first_column, standardized)

        return np.dot(self.params, std_input)

    def cost(self) -> float:
        X = self.mapped_training_set
        m = X.shape[0]
        y = self.target_set
        w = self.params

        prediction_diff = np.dot(X, w) - y
        square_cost = np.dot(prediction_diff.T, prediction_diff) / (2 * m)
        reg_cost = self.reg_lambda * np.dot(w, w) / (2 * m)

        return square_cost + reg_cost

    def gradient(self):
        X = self.mapped_training_set
        m = X.shape[0]
        y = self.target_set
        w = self.params

        primary_grad = np.dot(X.T, (np.dot(X, w) - y)) / m
        reg_grad = (self.reg_lambda / m) * w

        return primary_grad + reg_grad

    def learn_by_gradient_decent(self, learning_rate=0.01, epochs=100):
        self.cost_history = []
        for epoch in range(epochs):
            self.params -= learning_rate * self.gradient()
            self.cost_history.append(self.cost())

    def learn_by_normal_equation(self):
        X = self.mapped_training_set
        n = X.shape[1]
        y = self.target_set
        I_0 = np.identity(n)
        I_0[0, 0] = 0

        # 在正则化的情况下矩阵一定可逆，使用inv也是可以的。
        # 但在非正则化情况下，numpy实现inv的时候似乎有问题，
        # 有时候遇到singular矩阵也不报错，导致模型训练有问题，因此选择了pinv
        self.params = np.dot(np.dot(np.linalg.pinv(np.dot(X.T, X) + self.reg_lambda * I_0), X.T), y)
