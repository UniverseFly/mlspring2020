import numpy as np
from linear_models.binary_classification_model import RegularizedBinaryLogisticRegression


class MultipleClassificationModel:
    def __init__(self,
                 training_set: np.array,
                 target_set: np.array,
                 reg_lambda: float = 0.0,
                 basis_func=lambda x: x):
        self.training_set = training_set
        self.target_set = target_set
        self.reg_lambda = reg_lambda
        self.basis_func = basis_func
        self.learned_models = []

    def learn(self, learning_rate=0.01, epochs=300):
        categories = set(self.target_set)
        self.learned_models = []
        for c in categories:
            # 做一个映射，使one-vs-all可以利用二分类模型
            target_set = np.array(list(map(lambda yi: 1 if yi == c else 0, self.target_set)))
            model = RegularizedBinaryLogisticRegression(
                training_set=self.training_set,
                target_set=target_set,
                basis_func=self.basis_func,
                reg_lambda=self.reg_lambda
            )
            model.learn_by_gradient_decent(learning_rate, epochs)
            # 返回一个tuple，包含模型和它对应的类别
            self.learned_models.append((model, c))

    def prediction(self, input_data):
        max_index = 0
        max_value = self.learned_models[0][0].probability(input_data)
        for index in range(1, len(self.learned_models)):
            probability = self.learned_models[index][0].probability(input_data)
            if probability > max_value:
                max_value = probability
                max_index = index
        return self.learned_models[max_index][1]
